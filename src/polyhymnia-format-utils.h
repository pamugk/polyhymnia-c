
#pragma once

#include <glib-2.0/glib.h>

G_BEGIN_DECLS

gchar*
seconds_to_readable(guint duration);

G_END_DECLS
